package corp.abcmeasure.ruler.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import corp.abcmeasure.ruler.R;

/**
 * Documentation Referenced:
 * http://developer.android.com/reference/android/graphics/Canvas.html
 * http://developer.android.com/guide/practices/screens_support.html
 * http://developer.android.com/reference/android/content/res/Resources.html
 * http://developer.android.com/reference/android/view/View.html
 * <p/>
 * This is what our marking will look like.
 * <p/>
 * We want to show a number every 1 in and a marking every 1/16 in.
 * <p/>
 * We'll start 8dp from the left side so we're not on the corner.
 * | | | | | | | | | | | | | | | | |
 * |   |   |   |   |   |   |   |   |
 * |       |       |       |       |
 * |               |               |
 * |                               |
 * 0                               1
 */
public class RulerView extends View {

    // Styleable values
    private int mMarkTextSize;
    private int mMarkTextColor;
    private int mMarkTextYOffset;
    private int mMarkWidth;
    private int mMarkColor;
    private int mHandleWidth;
    private int mHandleColor;
    private int mHandleRadius;
    private int mMeasurementTextColor;
    private int mMeasurementTextSize;
    private int mMeasurementHintTextColor;
    private int mMeasurementHintTextSize;
    private int mMarkInchHeight;
    private int mMarkHalfInchHeight;
    private int mMarkFourthInchHeight;
    private int mMarkEighthHeight;
    private int mMarkSixteenthHeight;

    // Paints
    private Paint mMarkPaint;
    private Paint mMarkTextPaint;
    private Paint mHintTextPaint;
    private Paint mMeasurementPaint;
    private Paint mHandlePaint;

    // Handle
    private PointF mActiveHandle;

    // Sizes
    private int mWidth;
    private int mHeight;
    private int mZeroOffset = 0;

    public RulerView(Context context) {
        this(context, null);
    }

    public RulerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupAttrs(attrs);
        init();
    }

    private void setupAttrs(AttributeSet attrs) {

        // Load defaults
        final int defaultMarkTextSize = getResources().getDimensionPixelSize(R.dimen.default_mark_text_size);
        final int defaultMarkTextColor = ContextCompat.getColor(getContext(), R.color.default_mark_text_color);
        final int defaultMarkYOffset = getResources().getDimensionPixelSize(R.dimen.default_mark_text_y_offset);

        final int defaultMarkWidth = getResources().getDimensionPixelSize(R.dimen.default_mark_width);
        final int defaultMarkColor = ContextCompat.getColor(getContext(), R.color.default_mark_color);

        final int defaultHandleWidth = getResources().getDimensionPixelSize(R.dimen.default_handle_width);
        final int defaultHandleColor = ContextCompat.getColor(getContext(), R.color.default_handle_color);
        final int defaultHandleRadius = getResources().getDimensionPixelSize(R.dimen.default_handle_radius);

        final int defaultHintTextColor = ContextCompat.getColor(getContext(), R.color.default_hint_text_color);
        final int defaultHintTextSize = getResources().getDimensionPixelSize(R.dimen.default_hint_text_size);

        final int defaultMeasurementTextColor = ContextCompat.getColor(getContext(), R.color.default_measurement_color);
        final int defaultMeasurementTextSize = getResources().getDimensionPixelSize(R.dimen.default_measurement_text_size);

        final int defaultInchHeight = getResources().getDimensionPixelSize(R.dimen.default_mark_inch_height);
        final int defaultHalfInchHeight = getResources().getDimensionPixelSize(R.dimen.default_mark_half_inch_height);
        final int defaultFourthInchHeight = getResources().getDimensionPixelSize(R.dimen.default_mark_fourth_inch_height);
        final int defaultEighthHeight = getResources().getDimensionPixelSize(R.dimen.default_mark_eighth_inch_height);
        final int defaultSixteenthHeight = getResources().getDimensionPixelSize(R.dimen.default_mark_sixteenth_inch_height);

        final int defaultZeroOffset = getResources().getDimensionPixelSize(R.dimen.default_zero_offset);

        // Set styles
        final TypedArray styles = getContext().obtainStyledAttributes(attrs, R.styleable.RulerView);

        mMarkTextSize = styles.getDimensionPixelSize(R.styleable.RulerView_markTextSize, defaultMarkTextSize);
        mMarkTextColor = styles.getColor(R.styleable.RulerView_markTextColor, defaultMarkTextColor);
        mMarkTextYOffset = styles.getDimensionPixelSize(R.styleable.RulerView_markTextYOffset, defaultMarkYOffset);

        mMarkWidth = styles.getDimensionPixelSize(R.styleable.RulerView_markWidth, defaultMarkWidth);
        mMarkColor = styles.getColor(R.styleable.RulerView_markColor, defaultMarkColor);

        mHandleWidth = styles.getDimensionPixelSize(R.styleable.RulerView_handleWidth, defaultHandleWidth);
        mHandleColor = styles.getColor(R.styleable.RulerView_handleColor, defaultHandleColor);
        mHandleRadius = styles.getDimensionPixelSize(R.styleable.RulerView_handleRadius, defaultHandleRadius);

        mMeasurementHintTextColor = styles.getColor(R.styleable.RulerView_hintTextColor, defaultHintTextColor);
        mMeasurementHintTextSize = styles.getDimensionPixelSize(R.styleable.RulerView_hintTextSize, defaultHintTextSize);

        mMeasurementTextColor = styles.getColor(R.styleable.RulerView_measurementColor, defaultMeasurementTextColor);
        mMeasurementTextSize = styles.getDimensionPixelSize(R.styleable.RulerView_measurementTextSize, defaultMeasurementTextSize);

        mMarkInchHeight = styles.getDimensionPixelSize(R.styleable.RulerView_markInchHeight, defaultInchHeight);
        mMarkHalfInchHeight = styles.getDimensionPixelSize(R.styleable.RulerView_markHalfInchHeight, defaultHalfInchHeight);
        mMarkFourthInchHeight = styles.getDimensionPixelSize(R.styleable.RulerView_markFourthInchHeight, defaultFourthInchHeight);
        mMarkEighthHeight = styles.getDimensionPixelSize(R.styleable.RulerView_markEighthHeight, defaultEighthHeight);
        mMarkSixteenthHeight = styles.getDimensionPixelOffset(R.styleable.RulerView_markSixteenthHeight, defaultSixteenthHeight);

        mZeroOffset = styles.getDimensionPixelSize(R.styleable.RulerView_zeroOffset, defaultZeroOffset);

        styles.recycle();
    }

    private void init() {
        mMarkPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mMarkPaint.setColor(mMarkColor);
        mMarkPaint.setStrokeWidth(mMarkWidth);

        mMarkTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mMarkTextPaint.setColor(mMarkTextColor);
        mMarkTextPaint.setTextSize(mMarkTextSize);

        mHandlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mHandlePaint.setColor(mHandleColor);
        mHandlePaint.setStrokeWidth(mHandleWidth);
        mHandlePaint.setStyle(Paint.Style.FILL_AND_STROKE);

        mHintTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mHintTextPaint.setTextSize(mMeasurementHintTextSize);
        mHintTextPaint.setColor(mMeasurementHintTextColor);

        mMeasurementPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mMeasurementPaint.setColor(mMeasurementTextColor);
        mMeasurementPaint.setTextSize(mMeasurementTextSize);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN:
            case MotionEvent.ACTION_MOVE: {
                // Make sure our tap is inside the view with padding
                if (event.getX(0) >= mZeroOffset + getPaddingLeft()
                        && event.getX(0) <= mWidth - mZeroOffset - getPaddingRight()
                        && event.getY(0) >= getPaddingTop()
                        && event.getY(0) <= mHeight - getPaddingBottom()) {
                    if (mActiveHandle == null) {
                        mActiveHandle = new PointF(event.getX(0), event.getY(0));
                    }
                    mActiveHandle.x = event.getX(0);
                    mActiveHandle.y = event.getY(0);
                }
                break;
            }
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
                // Remove the handle on touch up, but only if it's the first finger
                if (event.getActionIndex() == 0) {
                    mActiveHandle = null;
                }
                break;
        }
        invalidate();

        return true;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mHeight = h;
        mWidth = w;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        ///////////////
        // Markings
        ///////////////

        // pixels per inch
        final float ppi = getResources().getDisplayMetrics().ydpi;

        // let's show a mark every 1/16
        final float mark = (ppi / 16);
        final int totalMarks = (int) ((mWidth - mZeroOffset - getPaddingLeft() - getPaddingRight()) / mark);
        float markPoint = 0;

        for (int i = 0; i < totalMarks; i++) {

            final int markSize = (i % 16 == 0) ? mMarkInchHeight
                    : (i % 8 == 0) ? mMarkHalfInchHeight
                    : (i % 4 == 0) ? mMarkFourthInchHeight
                    : (i % 2 == 0) ? mMarkEighthHeight
                    : mMarkSixteenthHeight;

            canvas.drawLine(mZeroOffset + markPoint + getPaddingLeft(), getPaddingTop(), mZeroOffset + markPoint + getPaddingLeft(), markSize, mMarkPaint);

            if (markSize == mMarkInchHeight) {
                String inchText = String.valueOf(Math.round(markPoint / ppi));
                canvas.drawText(inchText, mZeroOffset + markPoint + getPaddingLeft() - mMarkTextPaint.measureText(inchText) / 2, markSize + mMarkTextSize + mMarkTextYOffset, mMarkTextPaint);
            }

            markPoint += mark;
        }

        ///////////////
        // Handle
        ///////////////

        if (mActiveHandle != null) {
            // Draw our handle
            canvas.drawCircle(mActiveHandle.x, mActiveHandle.y, mHandleRadius, mHandlePaint);
            canvas.drawLine(mActiveHandle.x, mActiveHandle.y - mHandleRadius, mActiveHandle.x, getPaddingTop(), mHandlePaint);

            // Draw our measurement between 0 + zero offset and the line
            canvas.drawText(String.format("%.1f in", (mActiveHandle.x - mZeroOffset - getPaddingLeft()) / ppi), mZeroOffset + getPaddingLeft(), mHeight - mMeasurementTextSize - getPaddingBottom(), mMeasurementPaint);
        } else {
            canvas.drawText(getResources().getString(R.string.ruler_hint), mZeroOffset + getPaddingLeft(), mHeight - mMeasurementHintTextSize - getPaddingBottom(), mHintTextPaint);
        }

    }

    public int getMarkTextSize() {
        return mMarkTextSize;
    }

    public void setMarkTextSize(int markTextSize) {
        mMarkTextSize = markTextSize;
        mMarkTextPaint.setTextSize(markTextSize);
        invalidate();
    }

    public int getMarkTextColor() {
        return mMarkTextColor;
    }

    public void setMarkTextColor(@ColorRes int markTextColor) {
        mMarkTextColor = markTextColor;
        mMarkTextPaint.setColor(ContextCompat.getColor(getContext(), markTextColor));
        invalidate();
    }

    public int getMarkWidth() {
        return mMarkWidth;
    }

    public void setMarkWidth(int markWidth) {
        mMarkWidth = markWidth;
        mMarkPaint.setStrokeWidth(markWidth);
        invalidate();
    }

    public int getMarkColor() {
        return mMarkColor;
    }

    public void setMarkColor(@ColorRes int markColor) {
        mMarkColor = markColor;
        mMarkPaint.setColor(ContextCompat.getColor(getContext(), markColor));
        invalidate();
    }

    public int getHandleWidth() {
        return mHandleWidth;
    }

    public void setHandleWidth(int handleWidth) {
        mHandleWidth = handleWidth;
        mHandlePaint.setStrokeWidth(mHandleWidth);
        invalidate();
    }

    public int getHandleColor() {
        return mHandleColor;
    }

    public void setHandleColor(@ColorRes int handleColor) {
        mHandleColor = handleColor;
        mHandlePaint.setColor(ContextCompat.getColor(getContext(), handleColor));
        invalidate();
    }

    public int getHandleRadius() {
        return mHandleRadius;
    }

    public void setHandleRadius(int handleRadius) {
        mHandleRadius = handleRadius;
        invalidate();
    }

    public int getMeasurementTextColor() {
        return mMeasurementTextColor;
    }

    public void setMeasurementTextColor(@ColorRes int measurementTextColor) {
        mMeasurementTextColor = measurementTextColor;
        mMeasurementPaint.setColor(ContextCompat.getColor(getContext(), measurementTextColor));
        invalidate();
    }

    public int getMeasurementTextSize() {
        return mMeasurementTextSize;
    }

    public void setMeasurementTextSize(int measurementTextSize) {
        mMeasurementTextSize = measurementTextSize;
        mMeasurementPaint.setTextSize(measurementTextSize);
        invalidate();
    }

    public int getMeasurementHintTextColor() {
        return mMeasurementHintTextColor;
    }

    public void setMeasurementHintTextColor(@ColorRes int measurementHintTextColor) {
        mMeasurementHintTextColor = measurementHintTextColor;
        mHintTextPaint.setColor(ContextCompat.getColor(getContext(), measurementHintTextColor));
        invalidate();
    }

    public int getMeasurementHintTextSize() {
        return mMeasurementHintTextSize;
    }

    public void setMeasurementHintTextSize(int measurementHintTextSize) {
        mMeasurementHintTextSize = measurementHintTextSize;
        mHintTextPaint.setTextSize(measurementHintTextSize);
        invalidate();
    }

    public int getMarkInchHeight() {
        return mMarkInchHeight;
    }

    public void setMarkInchHeight(int markInchHeight) {
        mMarkInchHeight = markInchHeight;
        invalidate();
    }

    public int getMarkHalfInchHeight() {
        return mMarkHalfInchHeight;
    }

    public void setMarkHalfInchHeight(int markHalfInchHeight) {
        mMarkHalfInchHeight = markHalfInchHeight;
        invalidate();
    }

    public int getMarkFourthInchHeight() {
        return mMarkFourthInchHeight;
    }

    public void setMarkFourthInchHeight(int markFourthInchHeight) {
        mMarkFourthInchHeight = markFourthInchHeight;
        invalidate();
    }

    public int getMarkEighthHeight() {
        return mMarkEighthHeight;
    }

    public void setMarkEighthHeight(int markEighthHeight) {
        mMarkEighthHeight = markEighthHeight;
        invalidate();
    }

    public int getMarkSixteenthHeight() {
        return mMarkSixteenthHeight;
    }

    public void setMarkSixteenthHeight(int markSixteenthHeight) {
        mMarkSixteenthHeight = markSixteenthHeight;
        invalidate();
    }
}
